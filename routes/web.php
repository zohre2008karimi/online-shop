<?php

use App\Http\Controllers\ShowController;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


//صفحه اصلی

Route::get('/',[ShowController::class,'home']);

//فروشگاه
Route::get('/shop',[ShowController::class,'shop']);

//نظرات
Route::get('/comments',[ShowController::class,'comments']);

//نشان دادن رجیستر

Route::get('/register',[ShowController::class,'register']);



// auth routes

Route::post('/register',[RegisterController::class,'store'])->name('register');

Route::get('/login',[LoginController::class,'showLogin'])->name('login');

Route::redirect('login', '/loginForm');


Route::get('/cart',[LoginController::class,'showCart'])->middleware('auth');

Route::redirect('cart', '/cart')->middleware('auth');




