<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function showLogin()
    {
        return redirect('/loginForm');
    }
}
