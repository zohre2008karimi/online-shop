<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class ShowController extends Controller
{
    public function home(){

        return view('index');
    }


    public function shop(){

        return view('shop');
    }


    public function comments(){

        return view('comments');
    }



    public function register(){

        return view('register');
    }

}
