<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'=>'bail|required|string',
            'email'=>'bail|required|email|unique:users',
            'phone'=>'bail|required|integer|max:11|min:11',
            'username'=>'bail|required|string|max:15',
            'image'=>'bail|required|image',
            'image.*'=>'mimes:jpg,png,jpeg'
        ];
    }
}
