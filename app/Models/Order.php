<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'last_price',
        'count',
        'off',
    ];

      public function products()
      {
        return $this->belongsToMany(Product::class,'orders_products');
      }
}
