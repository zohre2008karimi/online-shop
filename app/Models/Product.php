<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Order;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'image',
        'price',
        'group',
        'off',
        'description',
   
    ];
    
    public function orders(){
        return $this->blongsToMany(Order::class,'orders_products');
      }
}
