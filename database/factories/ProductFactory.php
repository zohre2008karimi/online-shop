<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'image'=>$this->faker->imageUrl(),
            'name'=>$this->faker->name(),
            'price'=>$this->faker->numberBetween($min = 100000 , $max = 1000000),
            'off'=>$this->faker->numberBetween($min = 0 , $max = 100),
            'group'=>$this->faker->randomElement($array = array('history','science','culture','romance')),
            'count'=>$this->faker->numberBetween($min = 1 , $max =500),
            'description'=>$this->faker->realText(rand(50,70)),
        ];
    }
}
