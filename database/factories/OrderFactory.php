<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'last_price'=>$this->faker->numberBetween($min = 100000 , $max = 1000000),
            'off'=>$this->faker->numberBetween($min = 0 , $max = 100),
            'count'=>$this->faker->numberBetween($min = 1 , $max =500),
        ];
    }
}
