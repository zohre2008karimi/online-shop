<?php

namespace Database\Seeders;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Models\Comment;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
       User::factory(5)->create()->each(
        function($user)
        { 
            Comment::factory(5)->create(
                [
                    'user_id'=>$user->id,
                ],
            );
            $product = Product::factory(5)->create();
            Order::factory(5)->hasAttached($product , ['created_at'=>now() , 'updated_at'=>now()])->create(
    
                [
                    'user_id'=>$user->id,
                ],
            
      
            );
       
      });
   
  }
   

}