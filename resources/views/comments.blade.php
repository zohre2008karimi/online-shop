@extends('layout.master')

    @vite('resources/css/comments.css')


@section('content')
<br><br><br><br>
<br>
<br><br><br>
<form>
    <div class="form-group">
        <label for="subject">موضوع</label>
        <input type="text" id="subject" name="subject" placeholder="Enter subject">
    </div>
    <div class="form-group">
        <label for="comment">متن</label>
        <textarea id="comment" name="comment" placeholder="Enter comment"></textarea>
        <button type="submit">ثبت</button><br>
    </div>
    <br><br>
</form>

@endsection
