@extends('layout.master')
@section('content')
<!-- home section start -->
<section class="home" id="home">
    <div class="row">
        <div class="content">
            <h3>تا 75%  تخفیف ویژه</h3>
            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
            </p>
            <a href="#" class="btn">خرید این قالب در راستچین</a>
        </div>
        <div class="swiper books-slider">
            <div class="swiper-wrapper">
                <a href="#" class="swiper-slide"><img src="{{asset('image/book-1.png')}}" alt="کتاب 1"></a>
                <a href="#" class="swiper-slide"><img src="{{asset('image/book-2.png')}}" alt="کتاب 2"></a>
                <a href="#" class="swiper-slide"><img src="{{asset('image/book-3.png')}}" alt="کتاب 3"></a>
                <a href="#" class="swiper-slide"><img src="{{asset('image/book-4.png')}}" alt="کتاب 4"></a>
                <a href="#" class="swiper-slide"><img src="{{asset('image/book-5.png')}}" alt="کتاب 5"></a>
                <a href="#" class="swiper-slide"><img src="{{asset('image/book-6.png')}}" alt="کتاب 6"></a>
            </div>
            <img src="{{asset('image/stand.png')}}" class="stand" alt="">
        </div>
    </div>
</section>
<!-- home section end -->
<!-- icons section start -->
<section class="icons-container">
    <div class="icons">
        <i class="fas fa-shipping-fast"></i>
        <div class="content">
            <h3>ارسال رایگان</h3>
            <p>خرید بالای 300 هزار تومان</p>
        </div>
    </div>
    <div class="icons">
        <i class="fas fa-lock"></i>
        <div class="content">
            <h3>پرداخت امن</h3>
            <p>100درصد پرداخت امن</p>
        </div>
    </div>
    <div class="icons">
        <i class="fas fa-redo-alt"></i>
        <div class="content">
            <h3> ضمانت برگشت کالا</h3>
            <p>تا 7 روز ضمانت برگشت کالا</p>
        </div>
    </div>
    <div class="icons">
        <i class="fas fa-headset"></i>
        <div class="content">
            <h3>پشتیبانی 24 ساعته</h3>
            <p>در هر زمان که با ما تماس بگیرید</p>
        </div>
    </div>
    
</section>
<!-- icons section end -->
<!-- featured section start -->
<section class="featured" id="featured">
    <h1 class="heading"><span>تخفیف ویژه</span></h1>

    <div class="swiper featured-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-1.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-2.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-3.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-4.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-5.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-6.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                 <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-7.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-8.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-9.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="icons">
                    <a href="#" class="fas fa-search"></a>
                    <a href="#" class="fas fa-heart"></a>
                    <a href="#" class="fas fa-eye"></a>
                </div>
                <div class="image">
                    <img src="{{asset('image/book-10.png')}}" alt="">
                </div>
                <div class="content">
                    <h3>تخفیف  ویژه</h3>
                    <div class="price">15.000 تومان <span>20.000 تومان</span></div>
                    <a href="#" class="btn">افزودن به سبد خرید</a>
                </div>
            </div>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</section>
<!-- featured section end -->
<!-- newsletter section start -->
<section class="newesletter">
    <form action="">
        @csrf
        <h3>جهت اطلاع از جدیدترین  کتاب ها ما رو دنبال کنید</h3>
        <input type="email" name="" placeholder="ایمیل خود را وارد کنید" id="" class="box">
        <input type="submit" value="دنبال کردن"  class="btn">
    </form>
</section>
<!-- newsletter section end -->
<!-- arrivals section start -->
<section class="arrivals" id="arrivals">

    <h1 class="heading"> <span>جدید</span></h1>
    <div class="swiper arrivals-slider">

        <div class="swiper-wrapper">
            <a href="#" class="swiper-slide box">
             <div class="image">
                <img src="{{asset('image/book-1.png')}}" alt="">
             </div>
             <div class="content">
                <h3>کتاب جدید</h3>
                <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                <div class="stars">
                    <i class="fas fa-star-half-alt"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
              </div>
            </a> 

            <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-2.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 

               <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-3.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 
               <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-4.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 
               <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-5.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 
        </div>
    </div>
    <div class="swiper arrivals-slider">
        <div class="swiper-wrapper">
            <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-6.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 
               <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-7.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 
               <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-8.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 
               <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-9.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 
               <a href="#" class="swiper-slide box">
                <div class="image">
                   <img src="{{asset('image/book-10.png')}}" alt="">
                </div>
                <div class="content">
                   <h3>کتاب جدید</h3>
                   <div class="price">25.000 تومان <span>50.000 تومان</span></div>
                   <div class="stars">
                       <i class="fas fa-star-half-alt"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                       <i class="fas fa-star"></i>
                   </div>
                 </div>
               </a> 
        </div>
    </div>
</section>
<!-- arrivals section end -->

<a name="off"></a>
<!-- deal section start -->
<section class="deal">
    <div class="content">
        <h3> جشنواره تخفیف روز</h3>
        <h1>تا 50% تخفیف ویژه</h1>
        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
        </p>
        <a href="#" class="btn">  همین الان بخرید</a>
    </div>
    <div class="image">
        <img src="{{asset('image/deal-img.jpg')}}" alt="">
    </div>
</section>
<!-- deal section end -->

<!-- reviews section start -->
<section class="reviews" id="reviews">
    <h1 class="heading"><span>نظرات مشتریان</span></h1>
    <div class="swiper reviews-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide box">
                <img src="{{asset('image/pic-1.png')}}" alt="">
                <h3>مشتری آزمایشی</h3>
                <p>وقتی ثروت‌ های بزرگ به دست برخی مردم می‌افتد در پرتو آن نیرومند می‌شوند و در سایهٔ نیرومندی و ثروت خیال می‌ کنند که می‌توانند در خارج از وطن خود زندگی نمایند و خوشبخت و سرافراز باشند</p>
                <div class="stars">
                    <i class="fas fa-star-half-alt"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
            </div>
            <div class="swiper-slide box">
                <img src="{{asset('image/pic-2.png')}}" alt="">
                <h3>مشتری آزمایشی</h3>
                <p>وقتی ثروت‌ های بزرگ به دست برخی مردم می‌افتد در پرتو آن نیرومند می‌شوند و در سایهٔ نیرومندی و ثروت خیال می‌ کنند که می‌توانند در خارج از وطن خود زندگی نمایند و خوشبخت و سرافراز باشند</p>
                <div class="stars">
                    <i class="fas fa-star-half-alt"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
            </div>
            <div class="swiper-slide box">
                <img src="{{asset('image/pic-3.png')}}" alt="">
                <h3>مشتری آزمایشی</h3>
                <p>وقتی ثروت‌ های بزرگ به دست برخی مردم می‌افتد در پرتو آن نیرومند می‌شوند و در سایهٔ نیرومندی و ثروت خیال می‌ کنند که می‌توانند در خارج از وطن خود زندگی نمایند و خوشبخت و سرافراز باشند</p>
                <div class="stars">
                    <i class="fas fa-star-half-alt"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
            </div>
            <div class="swiper-slide box">
                <img src="{{asset('image/pic-4.png')}}" alt="">
                <h3>مشتری آزمایشی</h3>
                <p>وقتی ثروت‌ های بزرگ به دست برخی مردم می‌افتد در پرتو آن نیرومند می‌شوند و در سایهٔ نیرومندی و ثروت خیال می‌ کنند که می‌توانند در خارج از وطن خود زندگی نمایند و خوشبخت و سرافراز باشند</p>
                <div class="stars">
                    <i class="fas fa-star-half-alt"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
            </div>
            <div class="swiper-slide box">
                <img src="{{asset('image/pic-5.png')}}" alt="">
                <h3>مشتری آزمایشی</h3>
                <p>وقتی ثروت‌ های بزرگ به دست برخی مردم می‌افتد در پرتو آن نیرومند می‌شوند و در سایهٔ نیرومندی و ثروت خیال می‌ کنند که می‌توانند در خارج از وطن خود زندگی نمایند و خوشبخت و سرافراز باشند</p>
                <div class="stars">
                    <i class="fas fa-star-half-alt"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
            </div>
            <div class="swiper-slide box">
                <img src="{{asset('image/pic-6.png')}}" alt="">
                <h3>مشتری آزمایشی</h3>
                <p>وقتی ثروت‌ های بزرگ به دست برخی مردم می‌افتد در پرتو آن نیرومند می‌شوند و در سایهٔ نیرومندی و ثروت خیال می‌ کنند که می‌توانند در خارج از وطن خود زندگی نمایند و خوشبخت و سرافراز باشند</p>
                <div class="stars">
                    <i class="fas fa-star-half-alt"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- reviews section end -->

<!-- blogs section start -->
<section class="blogs" id="blogs">
    <h1 class="heading"><span>مقالات امروز</span></h1>

    <div class="swiper blogs-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide box">
                <div class="image">
                    <img src="{{asset('image/blog-1.jpg')}}" alt="">
                </div>
                <div class="content">
                    <h3>موضوع مقاله شما</h3>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                    </p>
                    <div class="content-2">
                        <h4>نویسنده : <span>عمر شهابی نیا</span></h4>
                    </div>
                    <a href="" class="btn"> ادامه مطلب</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="image">
                    <img src="{{asset('image/blog-2.jpg')}}" alt="">
                </div>
                <div class="content">
                    <h3>موضوع مقاله شما</h3>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                    </p>
                    <div class="content-2">
                        <h4>نویسنده : <span>عمر شهابی نیا</span></h4>
                    </div>
                    <a href="" class="btn"> ادامه مطلب</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="image">
                    <img src="{{asset('image/blog-3.jpg')}}" alt="">
                </div>
                <div class="content">
                    <h3>موضوع مقاله شما</h3>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                    </p>
                    <div class="conent-2">
                        <h4>نویسنده : <span>عمر شهابی نیا</span></h4>
                    </div>
                    <a href="" class="btn"> ادامه مطلب</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="image">
                    <img src="{{asset('image/blog-4.jpg')}}" alt="">
                </div>
                <div class="content">
                    <h3>موضوع مقاله شما</h3>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                    </p>
                    <div class="conent-2">
                        <h4>نویسنده : <span>عمر شهابی نیا</span></h4>
                    </div>
                    <a href="" class="btn"> ادامه مطلب</a>
                </div>
            </div>
            <div class="swiper-slide box">
                <div class="image">
                    <img src="{{asset('image/blog-5.jpg')}}" alt="">
                </div>
                <div class="content">
                    <h3>موضوع مقاله شما</h3>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                    </p>
                    <div class="conent-2">
                        <h4>نویسنده : <span>عمر شهابی نیا</span></h4>
                    </div>
                    <a href="" class="btn"> ادامه مطلب</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- blogs section end -->

@endsection