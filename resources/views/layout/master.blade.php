<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>bookly</title>
    <link rel="stylesheet"href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <!-- custom css file link  -->
  @vite('resources/css/style.css')
  
</head>
<body>

    <!-- POP-UP section START -->
{{-- <div id="miladWorkShopAlert" class="mwsModal">
	<div class="mwsModal-content">
		<div class="mwsModal-header">
			<span class="close">&times;</span>
			<h2>به فروشگاه بوکلی خوش آمدید</h2>
		</div>
		<div class="mwsModal-body">
			<p >
                کابر گرامی در راستای استفاده بهتر و آسان تر شما از خدمات بوکلی ، اقدام به طراحی اپلیکیشن شده که میتوانید جهت دانلود از لینک زیر استفاده یا در قسمت فوتر فروشگاه نسبت به دانلود آن اقدام کنید.
			</p>
            <a href="{{url('download')}}" class="btn"> دانلود اپلیکیشن</a>
		</div>
	</div>
</div --}}
<!-- POP-UP section END -->


<!-- header section starts  -->
<header class="header">
    <div class="header-1">
        <a href="#" class="logo"> Bookly <i class="fas fa-book"></i> </a>
        {{-- <form action="" class="search-form">
            <input type="search" name="" placeholder="جستجو کنید..." id="search-box">
            <label for="search-box" class="fas fa-search"></label>
        </form> --}}

        <div class="icons">
            {{-- <div id="search-btn" class="fas fa-search"></div> --}}
            <div  onclick="{{url('/cart')}}" id="shopping-btn" class="fas fa-shopping-cart"> </div>
           
          
            <div id="login-btn" class="fas fa-user"></div>
        </div>
    </div>
    <div class="header-2">
        <div class="support">
            <a href="#"><i class="fas fa-headset"></i> 021-1234</a>
        </div>
        <nav class="navbar">

          
            <a href="{{url('shop')}}">فروشگاه</a>
            <a href="{{url('orders')}}">سفارشات</a>
            <a href="{{url('new')}}">جدید</a>
            <a href="{{url('comments')}}">نظرات</a>
            <a href="https://www.karaketab.com/%DA%A9%D8%AA%D8%A7%D8%A8?limitstart=0">وبلاگ</a>

            <a href="#off">تخفیف ویژه</a>

        </nav>
    </div>
</header>
<!-- header section end  -->






<!-- bottom navbar starts  -->
<nav class="bottom-navbar">
    <a href="{{url('/')}}" class="fas fa-store"></a>
    <a href="{{url('featured')}}" class="fab fa-shopify"></a>
    <a href="{{url('arrivals')}}" class="fas fa-tags"></a>
    <a href="{{url('reviews')}}" class="fas fa-comments"></a>
    <a href="{{url('blogs')}}" class="fas fa-blog"></a>
</nav>
<!-------------------------------------------


{{header('Location:'.url('/loginForm'))}}
<!-LOGIN FORME-->
<div class="login-form-container">
    <div id="close-login-btn" class="fas fa-times"></div>
    <form action="">
        @csrf
        <h3>ورود /ثبت نام </h3>
        <span>شماره موبایل</span>
        <input type="text" name="text" class="box" placeholder=" /ایمیل/نام کاربری/شماره موبایل" id="">
      

        <button class="btn btn-info" >ارسال رمز</button>
        <input type="" name="code" class="box" placeholder=" رمز را وارد کنید" id="">


        <input type="submit" value="ورود" class="btn">

       
      
    </form>
</div>


<!-- basket FORME -->

{{header('Location:'.url('/cart'))}}
<div class="basket-contaner">
    <div id="close-shopping-btn" class="fas fa-times"></div>
    <h2 class="h-header">سبد خرید شما</h2>
    <div class="basket-content">
        <div class="sefaresh">
            <div class="sefaresh-1">
                <h2>پرستش خدایان<span>1 عدد</span></h2>
                <i class="fas fa-plus-circle" style="color: #27ae60;"></i>
                <i class="fas fa-minus-circle" style="color: red;"></i>
            </div>
            <div class="sefaresh-2">
                <h2>پرستش خدایان<span>1 عدد</span></h2>
                <i class="fas fa-plus-circle" style="color: #27ae60;"></i>
                <i class="fas fa-minus-circle" style="color: red;"></i>
            </div>
        </div>
        <div class="pric">
            <p>جمع: 25.000 تومان</p>
            <p>تخفیف: 5.000 تومان</p>
            <p>قابل پرداخت: 20.000 تومان</p>
        </div>
    </div>
    <div class="btn-btn">
        <a href="#" class="btn">تسویه حساب</a>
        <a href="{{url('home')}}" class="btn">ادامه خرید</a>
    </div>
</div>

@yield('content')


<!-- download section start -->

<section class="download-container" id="download">
    <div class="content">
        <i class="fas fa-book"></i>
        <h2>دانلود اپلیکیشن بوکلی...</h2>
    </div>
    <div class="box">
        <a href="#"><img src="{{asset('image/download/6f0c9aeb.svg')}}" alt=""></a>
        <a href="#"><img src="{{asset('image/download/b43d144f.svg')}}" alt=""></a>
        <a href="#"><img src="{{asset('image/download/c824b056.svg')}}" alt=""></a>
    </div>
</section>
<!-- download section end-->
<section class="footer">
    <div class="box-container">
        <div class="box">
            <h3>شعبه  ها</h3>
            <a href=""><i class="fas fa-map-marker-alt"></i>تهران</a>
            <a href=""><i class="fas fa-map-marker-alt"></i>مشهد</a>
            <a href=""><i class="fas fa-map-marker-alt"></i>اصفهان</a>
            <a href=""><i class="fas fa-map-marker-alt"></i>شیراز</a>
            <a href=""><i class="fas fa-map-marker-alt"></i>بندرعباس</a>
        </div>
        <div class="box">
            <h3>دسترسی سریع</h3>
            <a href=""><i class="fas fa-arrow-left"></i>فروشگاه</a>
            <a href=""><i class="fas fa-arrow-left"></i>تخفیف ویژه</a>
            <a href=""><i class="fas fa-arrow-left"></i>جدید</a>
            <a href=""><i class="fas fa-arrow-left"></i>نظرات</a>
            <a href=""><i class="fas fa-arrow-left"></i>مقالات</a>
            
        </div>
        <div class="box">
            <h3>راهنمای خرید از بوکلی</h3>
            <a href=""><i class="fas fa-arrow-left"></i>نحوه ثبت سفارش</a>
            <a href=""><i class="fas fa-arrow-left"></i>رویه ارسال سفارش</a>
            <a href=""><i class="fas fa-arrow-left"></i>شیوه های پرداخت</a>
            <a href=""><i class="fas fa-arrow-left"></i>قوانین بازگشت کالا</a>
            <a href=""><i class="fas fa-arrow-left"></i>پیگیری سفارش</a>
        </div>
        <div class="box">
            <h3>تماس با ما</h3>
            <a href=""><i class="fas fa-phone"></i>+123456789</a>
            <a href=""><i class="fas fa-phone"></i>+123456789</a>
            <a href=""><i class="fas fa-envelope"></i>bookly@info.com</a>
            <img src="{{asset('image/worldmap.png')}}" class="map">
        </div>
    </div>

        <div class="share">
            <a href="" class="fab fa-instagram"></a>
            <a href="" class="fab fa-whatsapp"></a>
            <a href="" class="fab fa-telegram"></a>
            <a href="" class="fab fa-youtube"></a>
            <a href="" class="fab fa-twitter"></a>
        </div>
        <div class="credit">طراحی و توسعه <i class="fab fa-instagram"></i><span><a href=""> عمر شهابی نیا</a></span></div>
</section>
<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
<!-- custom js file link  -->
@vite('resources/js/script.js')
</body>
</html>