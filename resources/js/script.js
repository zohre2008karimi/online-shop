// searchForm = document.querySelector('.search-form');
// document.querySelector('#search-btn').onclick = () =>{
//   searchForm.classList.toggle('active');
// }



// login events
let loginForm = document.querySelector('.login-form-container');

document.querySelector('#login-btn').onclick = () =>{
  loginForm.classList.toggle('active');
}
document.querySelector('#close-login-btn').onclick = () =>{
  loginForm.classList.remove('active');
}



let basketForm = document.querySelector('.basket-contaner');

document.querySelector('#shopping-btn').onclick = () =>{
  basketForm.classList.toggle('active');
}
document.querySelector('#close-shopping-btn').onclick = () =>{
  basketForm.classList.remove('active');
}




window.onscroll = () => {

  searchForm.classList.remove('active');
  if(window.scrollY > 80) {
    document.querySelector('.header-2').classList.add('active');
  }else{
    document.querySelector('.header-2').classList.remove('active');
  }
}

window.onload = () => {
  if(window.scrollY > 80) {
    document.querySelector('.header-2').classList.add('active');
  }else{
    document.querySelector('.header-2').classList.remove('active');
  }
}

var swiper = new Swiper(".books-slider", {
  loop: true,
  centeredSlides: true,
  autoplay: {
    delay: 9500,
    disabelOnInteraction: false,
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
    },
    768: {
      slidesPerView: 2,
    },
    1024: {
      slidesPerView: 3,
    },
  },
});

var swiper = new Swiper(".featured-slider", {
  loop: true,
  centeredSlides: true,
  autoplay: {
    delay: 9500,
    disabelOnInteraction: false,
  },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  breakpoints: {
    0: {
      slidesPerView: 1,
    },
    450: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 3,
    },
    1024: {
      slidesPerView: 4,
    },
  },
});


var swiper = new Swiper(".arrivals-slider", {
  spaceBetween: 10,
  loop: true,
  centeredSlides: true,
  autoplay: {
    delay: 9500,
    disabelOnInteraction: false,
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
    },
    768: {
      slidesPerView: 2,
    },
    1024: {
      slidesPerView: 3,
    },
  },
});

var swiper = new Swiper(".reviews-slider", {
  spaceBetween: 10,
  grabCursor: true,
  loop: true,
  centeredSlides: true,
  autoplay: {
    delay: 9500,
    disabelOnInteraction: false,
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
    },
    768: {
      slidesPerView: 2,
    },
    1024: {
      slidesPerView: 3,
    },
  },
});

var swiper = new Swiper(".blogs-slider", {
  spaceBetween: 10,
  grabCursor: true,
  loop: true,
  centeredSlides: true,
  autoplay: {
    delay: 9500,
    disabelOnInteraction: false,
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
    },
    768: {
      slidesPerView: 2,
    },
    1024: {
      slidesPerView: 3,
    },
  },
});



/*=================================================================*/

var mwsModal = document.getElementById('miladWorkShopAlert');
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];

span.onclick = function() {
    mwsModal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == mwsModal) {
        mwsModal.style.display = "none";
    }
}
/*==================================================================*/




